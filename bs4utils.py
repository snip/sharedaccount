#!/usr/bin/env python3

# Copyright © 2018, Snip <snip@kofuke.org>
#
# This file is part of SharedAccount.
#
# SharedAccount is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SharedAccount is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with SharedAccount.  If not, see <https://www.gnu.org/licenses/>.

from bs4 import BeautifulSoup

def html2payload(html):
    payload = {}
    soup = BeautifulSoup(html, 'html.parser')
    for input in soup.find_all('input'):
        payload[input.get('name')] = input.get('value')
    for select in soup.find_all('select'):
        selected_options = select.find_all('option', selected=True)
        if selected_options:
            payload[select.get('name')] = selected_options[0].get('value')
        else:
            options = select.find_all('option')
            payload[select.get('name')] = options[0].get('value')
    return payload

