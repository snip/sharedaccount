#!/usr/bin/env python3

# Copyright © 2018, Snip <snip@kofuke.org>
#
# This file is part of SharedAccount.
#
# SharedAccount is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SharedAccount is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with SharedAccount.  If not, see <https://www.gnu.org/licenses/>.

reverse_domain = "example.net"
real_domain = "example.com"
headers_to_add = {
    "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0",
    "DNT": "1",
    "Upgrade-Insecure-Requests": "1",
}
headers_to_delete = ["X-Forwarded-Proto", "X-Forwarded-For", "X-Forwarded-Host", "X-Forwarded-Server", "Authorization"]
cookie_name = "sessionid"
autologin = {
    "form_path_regex": "^/login/$",
    "submit_url": "https://example.com/login/",
    "redirect_path": "/home/",
    "headers_to_add": {
        "Referer": "https://example.com/login/",
    },
    "params": {
        "username": "myuser",
        "password": "mypass",
    },
}

