#!/usr/bin/env python3

# Copyright © 2018, Snip <snip@kofuke.org>
#
# This file is part of SharedAccount.
#
# SharedAccount is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SharedAccount is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with SharedAccount.  If not, see <https://www.gnu.org/licenses/>.

import sharedaccountconfig as cfg
import re
import requests
import bs4utils

class SharedAccount:
    def __init__(self):
        self.real_domain = cfg.real_domain
        self.reverse_domain = cfg.reverse_domain
        self.headers_to_add = cfg.headers_to_add
        self.headers_to_delete = cfg.headers_to_delete
        self.cookie_name = cfg.cookie_name
        self.autologin = cfg.autologin
        self.cookie = ""

    def request(self, flow):
        # Headers (set, delete and domain conversion)
        for k in self.headers_to_add:
            flow.request.headers[k] = self.headers_to_add[k]
        for k in flow.request.headers:
            flow.request.headers[k] = flow.request.headers[k].replace(self.reverse_domain, self.real_domain)
        for k in self.headers_to_delete:
            if k in flow.request.headers:
                del flow.request.headers[k]

        # Define req_cookie
        req_cookie = ""
        if "Cookie" in flow.request.headers:
            m = re.search(self.cookie_name + "=([^;]*)", flow.request.headers["Cookie"])
            if m:
                req_cookie = m.group(1)

        # Shared cookie management
        if (self.cookie != "" and req_cookie != self.cookie):
            # req_cookie = self.cookie
            if (req_cookie != ""):
                flow.request.headers["Cookie"] = flow.request.headers["Cookie"].replace(req_cookie, self.cookie)
            elif "Cookie" in flow.request.headers and flow.request.headers["Cookie"] != "":
                flow.request.headers["Cookie"] = flow.request.headers["Cookie"] + "; " + self.cookie_name + "=" + self.cookie
            else:
                flow.request.headers["Cookie"] = self.cookie_name + "=" + self.cookie
        elif self.cookie == "" and req_cookie != "":
            self.cookie = req_cookie

    def response(self, flow):
        # Domain conversion in headers and content 
        for k in flow.response.headers:
            flow.response.headers[k] = flow.response.headers[k].replace(self.real_domain, self.reverse_domain)
        flow.response.content = flow.response.content.replace(self.real_domain.encode("utf-8"), self.reverse_domain.encode("utf-8"))

        # Define res_cookie
        res_cookie = ""
        if "Set-Cookie" in flow.response.headers:
            m = re.search(self.cookie_name + "=([^;]*)", flow.response.headers["Set-Cookie"])
            if m:
                res_cookie = m.group(1)

        # Shared cookie management
        if res_cookie != "":
            self.cookie = res_cookie

        # Autologin
        if re.search(self.autologin["form_path_regex"], flow.request.path):
            payload = bs4utils.html2payload(flow.response.content)
            for key, value in self.autologin["params"].items():
                payload[key] = value
            merged_headers = {**flow.request.headers, **self.autologin["headers_to_add"]}
            r = requests.post(self.autologin["submit_url"], headers=merged_headers, data=payload)
            autologin_cookie = ""
            if "Set-Cookie" in r.headers:
                m = re.search(self.cookie_name + "=([^;]*)", r.headers["Set-Cookie"])
                if m:
                    autologin_cookie = m.group(1)
            if autologin_cookie != "":
                self.cookie = autologin_cookie
                flow.response.text = ""
                flow.response.status_code = 302
                flow.response.headers["Location"] = self.autologin["redirect_path"]


addons = [
    SharedAccount()
]

