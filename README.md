# sharedaccount

L'objectif de ce programme est de permettre l'utilisation d'un service par plusieurs personnes avec un compte partagé.

## Fonctionnement
Ce programme est fourni en tant qu'extention de `mitmprox`. Le principe est de mettre en place un reverse-proxy qui sera en charge de mutualiser le cookie de session.

## Configuration
La configuration doit se trouver dans le fichier `sharedaccountconfig.py`.
```
cp sharedaccountconfig.example.py sharedaccountconfig.py
```

Les trois principaux paramètres sont les suivants :
```
reverse_domain = "example.net"
real_domain = "example.com"
cookie_name = "sessionid"
```

## Connexion automatique
Il est possible d'activer un mécanisme de connexion automatique en remplissant les informations suivantes :
```
autologin = {
    "form_path_regex": "^/login/$",
    "submit_url": "https://example.com/login/",
    "redirect_path": "/home/",
    "headers_to_add": {
        "Referer": "https://example.com/login/",
    },
    "params": {
        "username": "myuser",
        "password": "mypass",
    },
}
```
